﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ServicioPuntosBonus
{
    class Program
    {
        static void Main(string[] args)
        {
            var exitCode = HostFactory.Run(x =>
            {
                x.Service<PuntosBonus>(s =>
                {
                s.ConstructUsing(puntosbonus => new PuntosBonus());
                s.WhenStarted(puntosbonus => puntosbonus.Start());
                s.WhenStopped(puntosbonus => puntosbonus.Stop());
                });

                x.RunAsLocalSystem();
                x.SetServiceName("ServicioPuntosBonus");
                x.SetDisplayName("Servicio de Puntos Bonus");
                x.SetDescription("Servicio que envía acumulaciones mediante webservice para documentos en contingencia");

            });

            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
        }
    }
}
