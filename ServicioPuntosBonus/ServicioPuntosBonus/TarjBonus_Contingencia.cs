﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioPuntosBonus
{
    public class TarjBonus_Contingencia
    {
        public string cdestacion { get; set; }
        public string nrobonus { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string nroequipo { get; set; }
        public string nrotransac { get; set; }
        public string detallexml { get; set; }
        public string totalvta { get; set; }
        public bool enviado { get; set; }

    }
}
