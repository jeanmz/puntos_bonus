﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Data;

namespace ServicioPuntosBonus
{
    public class DataAcces
    {
        public List<TarjBonus_Contingencia> ListarTransacciones()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("Connection")))
            {
                var output = connection.Query<TarjBonus_Contingencia>("dbo.SP_BONUS_LISTAR_TRANSACCIONES").ToList();
                return output;
            }
        }

        public void ActualizarTransaccion(string Pos_Id, string Pos_Secuencial)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("Connection")))
            {
                //TarjBonus_Contingencia transaccion = new TarjBonus_Contingencia();

                //transaccion.nroequipo = Pos_Id;
                //transaccion.nrotransac = Pos_Secuencial;

                connection.Execute("dbo.SP_BONUS_ACTUALIZAR_TRANSACCION @nroequipo, @nrotransac", new { nroequipo = Pos_Id, nrotransac = Pos_Secuencial });


            }
        }
    }
}
