﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using System.Configuration;

namespace ServicioPuntosBonus
{
    public class PuntosBonus
    {
        List<TarjBonus_Contingencia> transacciones = new List<TarjBonus_Contingencia>();
        WSPuntosBonus.WSAcumuPxSoapPortClient wsbonus = new WSPuntosBonus.WSAcumuPxSoapPortClient();

        private readonly Timer _timer;

        public PuntosBonus()
        {

            if (!(Directory.Exists(@"C:\Bonus\Log")))
            {
                Directory.CreateDirectory(@"C:\Bonus\Log");
            }

            if (!System.IO.File.Exists(@"C:\Bonus\Log\Transacciones.txt"))
            {
                System.IO.File.Create(@"C:\Bonus\Log\Transacciones.txt");
            }

            int minutos = Int32.Parse(ConfigurationManager.AppSettings["minutos"].ToString());
            int milisegundos = minutos * 60000;
            _timer = new Timer(milisegundos) { AutoReset = true };
            _timer.Elapsed += TimerElapsed;

        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            DataAcces db = new DataAcces();
            transacciones = db.ListarTransacciones();

            if (transacciones.Count > 0)
            {
                foreach (var item in transacciones)
                {

                    string Comercio = item.cdestacion;
                    string Tarjetac11 = item.nrobonus;
                    string Fechatransaccion = item.fecha;
                    string Horatransaccion = item.hora;
                    string Pos_id = item.nroequipo;
                    string Pos_secuencial = item.nrotransac;
                    string Lineasdetalle = item.detallexml;
                    string Totalsolestrn = item.totalvta;
                    string Flagoperacion = "0";

                    try
                    {
                        var respuesta = wsbonus.Execute(Comercio, Tarjetac11, Fechatransaccion, Horatransaccion, Pos_id, Pos_secuencial, Lineasdetalle, Totalsolestrn, Flagoperacion, out string Flagretorno);

                        string respuestaTxt = DateTime.Now.ToString() + "|" + Comercio + "|" + Tarjetac11 + "|" + Fechatransaccion + "|" + Horatransaccion + "|" + Pos_id + "|" + Pos_secuencial + "|" + Lineasdetalle + "|" + Totalsolestrn + " => " + respuesta;

                        string[] lines = new string[] { respuestaTxt };
                        
                        File.AppendAllLines(@"C:\Bonus\Log\Transacciones.txt", lines);

                        db.ActualizarTransaccion(Pos_id, Pos_secuencial);
                    }
                    catch (Exception ex)
                    {
                        string exception = ex.InnerException.Message;

                        string respuestaTxt = DateTime.Now.ToString() + "|" + Comercio + "|" + Tarjetac11 + "|" + Fechatransaccion + "|" + Horatransaccion + "|" + Pos_id + "|" + Pos_secuencial + "|" + Lineasdetalle + "|" + Totalsolestrn + " => " + exception;

                        string[] lines = new string[] { respuestaTxt };

                        File.AppendAllLines(@"C:\Bonus\Log\Transacciones.txt", lines);
                    }

                }
            }
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

    }
}
