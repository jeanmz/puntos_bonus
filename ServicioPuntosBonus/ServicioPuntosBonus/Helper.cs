﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioPuntosBonus
{
    public static class Helper
    {
        public static string CnnVal(string Connection)
        {
            return ConfigurationManager.ConnectionStrings[Connection].ConnectionString;
        }
    }
}
