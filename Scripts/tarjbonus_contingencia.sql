USE [BACKOFFICE]
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'tarjbonus_contingencia') 
BEGIN
	DROP TABLE [dbo].[tarjbonus_contingencia]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tarjbonus_contingencia](
	[cdestacion] [char](6) NOT NULL,
	[nrobonus] [char](11) NOT NULL,
	[fecha] [char](8) NOT NULL,
	[hora] [char](4) NOT NULL,
	[nroequipo] [char](6) NOT NULL,
	[nrotransac] [char](6) NOT NULL,
	[detallexml] [varchar](max) NOT NULL,
	[totalvta] [char](7) NOT NULL,
	[enviado] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


