USE [BACKOFFICE]
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SP_BONUS_LISTAR_TRANSACCIONES') 
BEGIN
	DROP PROC [dbo].[SP_BONUS_LISTAR_TRANSACCIONES]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SP_BONUS_LISTAR_TRANSACCIONES]
AS
	SELECT * FROM tarjbonus_contingencia WHERE enviado = 0
GO


