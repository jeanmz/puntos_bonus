USE [BACKOFFICE]
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SP_BONUS_ACTUALIZAR_TRANSACCION') 
BEGIN
	DROP PROC [dbo].[SP_BONUS_ACTUALIZAR_TRANSACCION]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SP_BONUS_ACTUALIZAR_TRANSACCION]
	@nroequipo CHAR(6),
	@nrotransac CHAR(6)
AS
BEGIN
	UPDATE tarjbonus_contingencia SET enviado = 1 WHERE nroequipo = @nroequipo AND nrotransac = @nrotransac
	UPDATE tarjbonus SET enviado = 1 WHERE nroequipo = @nroequipo AND nrotransac = @nrotransac
END
GO